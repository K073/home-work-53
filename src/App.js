import React, { Component } from 'react';
import './App.css';
import Task from "./Task/Task";

class App extends Component {

  constructor (props) {
    super(props);
    this.state = {
      arrTask: [
          "Buy milk",
          "Walk with dog",
          "Do homework"
      ],
      inputValue: ''
    };
  }

  removeTask = (index) => {
    const arrTask = [...this.state.arrTask];
    arrTask.splice(index, 1);

    this.setState({arrTask})
  };

  getValue = (event) => {
    this.setState({inputValue : event.target.value});
  };

  addTask = (event) => {
    event.preventDefault();

    if (this.state.inputValue !== ''){
        const arrTask = [...this.state.arrTask];
        arrTask.push(this.state.inputValue);
        this.setState({arrTask});
    }
  };
  
  render() {
    let list = this.state.arrTask.map((value, index) => {
      return <Task key={index} text={value} removeTask={() => this.removeTask(index)}/>
    });
    return (
      <div className="App">
          <form action="" onSubmit={this.addTask}>
              <input type="text" onChange={this.getValue}/>
              <input type="submit" value="add"/>
          </form>
          <ul className={"Task-list"}>{list}</ul>
      </div>
    );
  }
}

export default App;
