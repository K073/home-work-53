import React from 'react';
import './Task.css'

const Task = (props) =>{
    return (
        <li className={"Task"}>
            {props.text}<button className={"Task-remove"} onClick={props.removeTask}>remove</button>
        </li>
    );
};

export default Task;